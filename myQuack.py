
'''

Scaffolding code for the Machine Learning assignment. 

You should complete the provided functions and add more functions and classes as necessary.
 
Write a main function that calls the different functions to perform the required tasks 
and repeat your experiments.


'''

import csv
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn import preprocessing
import matplotlib.pyplot as plt
import time

SEED = 123456 # Random state seed for repeatable results
CV_FOLDS = 20 # Number of folds in cross validation scoring

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def my_team():
    '''
    Return the list of the team members of this assignment submission as a list
    of triplet of the form (student_number, first_name, last_name)
    '''
    return [ (9215719, 'Daniel', 'Wan'), (9182004, 'Jason', 'Chang') ]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def prepare_dataset(dataset_path):
    '''  
    Read a comma separated text file where 
	- the first field is a ID number 
	- the second field is a class label 'B' or 'M'
	- the remaining fields are real-valued

    Return two numpy arrays X and y where 
	- X is two dimensional. X[i,:] is the ith example
	- y is one dimensional. y[i] is the class label of X[i,:]
          y[i] should be set to 1 for 'M', and 0 for 'B'

    @param dataset_path: full path of the dataset text file

    @return
	X,y
    '''
    examples = []
    class_labels = []
    
    with open(dataset_path, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            examples.append(row[2:])
            class_labels.append(1 if row[1] == 'M' else 0)
    
    X = np.array(examples, dtype = 'float64')
    y = np.array(class_labels, dtype = 'float64') 
    return X, y

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def build_NB_classifier(X_training, y_training, reporting = False):
    '''  
    Build a Naive Bayes classifier based on the training set X_training, y_training.

    @param 
	X_training: X_training[i,:] is the ith example
	y_training: y_training[i] is the class label of X_training[i,:]
   reporting: Whether to report calculated values

    @return
	clf : the classifier built in this function
    '''
    startTime = time.time()
    
    clf = GaussianNB()
    clf.fit(X_training, y_training)    
    
    if reporting:
        print('Execution Time :', str(time.time() - startTime))
        print('Training Error :', 1.0 - clf.score(X_training, y_training))
    
    return clf
    
    
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

MAX_TESTED_MAX_DEPTH = 10 # Tested values are from 1 to this value
# This value was selected experimentally to produce values close to the best

def build_DT_classifier(X_training, y_training, reporting = False):
    '''  
    Build a Decision Tree classifier based on the training set X_training, y_training.

    @param 
	X_training: X_training[i,:] is the ith example
	y_training: y_training[i] is the class label of X_training[i,:]
	reporting: Whether to report and plot calculated values

    @return
	clf : the classifier built in this function
    '''    
    startTime = time.time()
    
    testVals = list(range(1, MAX_TESTED_MAX_DEPTH + 1))
    scores = []
    
    # Get cross validation score means for max depths in specified test range
    for depth in testVals:
        clf = DecisionTreeClassifier(max_depth = depth, random_state = SEED)
        scores.append(cross_val_score(clf, X_training, y_training, cv = CV_FOLDS).mean())
    
    # Get best scoring max depth
    bestMaxDepth = testVals[scores.index(max(scores))]
    
    # Return classifier built using best value
    clf = DecisionTreeClassifier(max_depth = bestMaxDepth, random_state = SEED)
    clf.fit(X_training, y_training)
    
    if reporting:
        print('Execution Time :', str(time.time() - startTime))
        print('Best Max Depth :', bestMaxDepth)
        print('Validation Error :', 1.0 - max(scores))
        print('Training Error :', 1.0 - clf.score(X_training, y_training))
        # Plot CVS means against tested max depths
        plot(testVals, scores, 'Max Depth', 'Mean CVS', 'Decision Tree Classifier')
        
    return clf

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

MAX_TESTED_NUM_NEIGHBOURS = 30 # Tested values are from 1 to this value
# This value was selected experimentally to produce values close to the best

def build_NN_classifier(X_training, y_training, reporting = False):
    '''  
    Build a Nearrest Neighbours classifier based on the training set X_training, y_training.

    @param 
	X_training: X_training[i,:] is the ith example
	y_training: y_training[i] is the class label of X_training[i,:]
	reporting: Whether to report and plot calculated values

    @return
	clf : the classifier built in this function
    '''
    startTime = time.time()
    
    testVals = list(range(1, MAX_TESTED_NUM_NEIGHBOURS + 1))
    scores = []
    
    # Get cross validation score means for numbers of neighbours in specified test range
    for numNeighbours in testVals:
        clf = KNeighborsClassifier(numNeighbours)
        scores.append(cross_val_score(clf, X_training, y_training, cv = CV_FOLDS).mean())
    
    # Get best scoring neighbour number
    bestNumNeighbours = testVals[scores.index(max(scores))]
    
    # Return classifier built using best value   
    clf = KNeighborsClassifier(bestNumNeighbours)
    clf.fit(X_training, y_training)
    
    if reporting:
        print('Execution Time :', str(time.time() - startTime))
        print('Best No. of Neighbours :', bestNumNeighbours)
        print('Validation Error :', 1.0 - max(scores))
        print('Training Error :', 1.0 - clf.score(X_training, y_training))
        # Plot CVS means against tested max depths
        plot(testVals, scores, 'No. of Nearest Neighbours', 'Mean CVS', 'Nearest Neighbours Classifier')
    
    return clf

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

MAX_TESTED_C_VALUE = 3 # Tested values are from (10 ** -this) to (10 ** this)
# This value was selected experimentally to produce values close to the best

def build_SVM_classifier(X_training, y_training, reporting = False):
    '''  
    Build a Support Vector Machine classifier based on the training set X_training, y_training.

    @param 
	X_training: X_training[i,:] is the ith example
	y_training: y_training[i] is the class label of X_training[i,:]
	reporting: Whether to report and plot calculated values

    @return
	clf : the classifier built in this function
    '''
    startTime = time.time()
    
    powers = np.linspace(-1 * MAX_TESTED_C_VALUE, MAX_TESTED_C_VALUE, 50)
    testVals = list(10 ** x for x in powers)
    scores = []
    
    # Get cross validation score means for values of C in specified test range
    for CValue in testVals:
        clf = SVC(C = CValue, random_state = SEED)
        scores.append(cross_val_score(clf, X_training, y_training, cv = CV_FOLDS).mean())
      
    # Get best scoring C value
    bestCValue = testVals[scores.index(max(scores))]
    
    # Return classifier built using best value   
    clf = SVC(C = bestCValue, random_state = SEED)
    clf.fit(X_training, y_training)
    
    if reporting:
        print('Execution Time :', str(time.time() - startTime))
        print('Best Penalty (C) Value :', bestCValue)
        print('Validation Error :', 1.0 - max(scores))
        print('Training Error :', 1.0 - clf.score(X_training, y_training))
        # Plot CVS means against tested C values
        plot(testVals, scores, 'Penalty (C) Value', 'Mean CVS', 'Support Vector Machine Classifier')
        plt.xscale('log') # Log scale for C values
    
    return clf

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def plot(x, y, x_label, y_label, title):
    '''
    Plot x against y with given axis labels and title.
    
    @param
	x: Horizontal axis data points
	y: Vertical axis data points
	x_label: Horizontal axis label
	y_label: Vertical axis label
	title: Title of the plot
    '''
    fig = plt.figure()
    fig.suptitle(title)
    plt.plot(x, y)
    yRange = max(y) - min(y)
    plt.axis((min(x), max(x), min(y) - 0.1 * yRange, max(y) + 0.1 * yRange))
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

TRAIN_DATA_RATIO = 0.8 # Proportion of the data used as the training set
# The remainder of the data is used as the testing set

def main():
    X, y = prepare_dataset('medical_records.data')
    
    # Split dataset into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = SEED,
            train_size = TRAIN_DATA_RATIO, test_size = 1.0 - TRAIN_DATA_RATIO)
    
    # Standardization required for SVM classifier and may benefit other classifiers
    # Training and tesing sets are scaled separately to avoid testing set 'contamination'
    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    
    # Score function for all classifiers is accuracy
    
    print('------------- Naive Bayes -------------')
    NB = build_NB_classifier(X_train, y_train, reporting = True)
    print('Testing Error :', 1.0 - NB.score(X_test, y_test))
    print()
    
    print('------------- Decision Tree -------------')
    DT = build_DT_classifier(X_train, y_train, reporting = True)
    print('Testing Error :', 1.0 - DT.score(X_test, y_test))
    print()
    
    print('------------- Nearest Neighbours -------------')
    NN = build_NN_classifier(X_train, y_train, reporting = True)
    print('Testing Error :', 1.0 - NN.score(X_test, y_test))
    print()
    
    print('------------- Support Vector Machine -------------')
    SVM = build_SVM_classifier(X_train, y_train, reporting = True)
    print('Testing Error :', 1.0 - SVM.score(X_test, y_test))
    print()

if __name__ == "__main__":
    main()